<?php

namespace Magento\Payl8rPaymentGateway\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Payment\Gateway\ConfigInterface;
use Magento\Sales\Model\OrderRepository;
use Magento\Payment\Model\Method\Logger;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Data extends AbstractHelper {

  private $orderRepository;
  private $config;
  private $logger;
  private $encryptor;
  private $orderSender;
  private $searchCriteriaBuilder;

  /**
   *
   * @param Context $context
   * @param OrderRepository $orderRepository
   * @param ConfigInterface $config
   * @param EncryptorInterface $encryptor
   * @param Logger $logger
   */
  public function __construct(
  Context $context, OrderRepository $orderRepository, OrderSender $orderSender, ConfigInterface $config, EncryptorInterface $encryptor, Logger $logger, SearchCriteriaBuilder $searchCriteriaBuilder,
  ProductRepositoryInterface $productRepositoryInterface
  ) {
    $this->orderRepository = $orderRepository;
    $this->orderSender = $orderSender;
    $this->config = $config;
    $this->encryptor = $encryptor;
    $this->logger = $logger ?: ObjectManager::getInstance()->get(LoggerInterface::class);
    $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    $this->productRepositoryInterface = $productRepositoryInterface;
    parent::__construct($context);
  }

  public function prepareIframeData($orderId) {
    $order = $this->orderRepository->get($orderId);

    $billing = $order->getBillingAddress();
    $shipping = $order->getShippingAddress();
    if (empty($billing)) {
      $billing = $shipping;
    }

    $test = $this->config->getValue('test_mode', $order->getStoreId());
    $username = $this->config->getValue('merchant_username', $order->getStoreId());
    $publicKey = $this->config->getValue('merchant_gateway_key', $order->getStoreId());

    $abortUrl = $this->_urlBuilder->getUrl('payl8rpaymentgateway/payment/cancel');
    $failUrl = $this->_urlBuilder->getUrl('payl8rpaymentgateway/payment/cancel');
    $successUrl = $this->_urlBuilder->getUrl('checkout/onepage/success');
    $returnUrl = $this->_urlBuilder->getUrl('payl8rpaymentgateway/payment/response');

    $products = [];
    foreach ($order->getItems() as $product) {
      // don't get parent items
      if (!$product->getHasChildren()) {
        $products[] = $product->getName();
      }
    }

    $product_description = implode("<br>", $products);
    if (strlen($product_description) > 79) {
      $product_description = 'Your order of ' . count($products) . ' items.';
    }


    $items = [];
    foreach ($order->getAllItems() as $item) {
      $item_data = [];
      $item_data['description'] = $item->getName();
      $item_data['quantity'] = $item->getQtyOrdered();
      $item_data['price'] = $item->getQtyOrdered()*($item->getPrice());
      $items[] = $item_data;
    }
    //prepare shipping ammount
    $item_data = [];
    $item_data['description'] = 'Shipping Costs';
    $item_data['quantity'] = '';
    $item_data['price'] = floatval($order->getShippingAmount());
    $items[] = $item_data;
    //prepare Tax ammount
    $item_data = [];
    $item_data['description'] = 'Tax';
    $item_data['quantity'] = '';
    $item_data['price'] = floatval($order->getTaxAmount());
    $items[] = $item_data;
    //Check if there has been a discount, if so, include it in final pricing
    if (abs(floatval($order->getDiscountAmount())) > 0){
        $item_data = [];
        $item_data['description'] = 'Discounts';
        $item_data['quantity'] = '';
        $item_data['price'] = floatval($order->getDiscountAmount());
        $items[] = $item_data;
    }
    $item_json = json_encode($items, TRUE);
    $e_item_json = [];

    $split_json_strings = str_split($item_json, 800);
    foreach($split_json_strings as $split_json_string) {

       // encrypt each split string
       openssl_public_encrypt($split_json_string, $e_item_json_temp, $publicKey);

       // add each encrypted string to e_item_json array
       $e_item_json[] = base64_encode($e_item_json_temp);
    }

    $data = array(
        "username" => $username,
        "request_data" => array(
            "return_urls" => array(
                "abort" => str_replace('http:', 'https:', $abortUrl),
                "fail" => str_replace('http:', 'https:', $failUrl),
                "success" => str_replace('http:', 'https:', $successUrl),
                "return_data" => str_replace('http:', 'https:', $returnUrl),
            ),
            "request_type" => "standard_finance_request",
            "test_mode" => (int) $test,
            "order_details" => array(
                "order_id" => $order->getIncrementId(),
                "currency" => "GBP",
                "total" => floatval($order->getGrandTotal())
            ),
            "customer_details" => array(
                "student" => 0,
                "firstnames" => $billing ? $billing->getFirstname() : '',
                "surname" => $billing ? $billing->getLastname() : '',
                "email" => $billing ? $billing->getEmail() : '',
                "phone" => $billing ? $billing->getTelephone() : '',
                "address" => $billing ? $billing->getStreetLine(1) : '',
                "city" => $billing ? $billing->getCity() : '',
                "country" => "UK",
                "postcode" => $billing ? str_replace(' ', '', $billing->getPostcode()) : '',
            ),
            "api_version" => "1.1"
        )
    );

    if (!empty($shipping) && !$shipping->getSameAsBilling()) {
      $data["request_data"]["customer_details"]["delivery_name"] = $shipping->getFirstname() . ' ' . $shipping->getLastname();
      $data["request_data"]["customer_details"]["delivery_address"] = $shipping->getStreetLine(1);
      $data["request_data"]["customer_details"]["delivery_city"] = $shipping->getCity();
      $data["request_data"]["customer_details"]["delivery_postcode"] = $shipping->getPostcode();
      $data["request_data"]["customer_details"]["delivery_country"] = "UK";
    }

    $json_data = json_encode($data);
    openssl_public_encrypt($json_data, $crypted, $publicKey);


    return array(
      'rid' => $username,
      'data' => base64_encode($crypted),
      'item_data' => $e_item_json,
      'action' => 'https://payl8r.com/process'
    );
  }

  public function processResponse( $response ) {

    $this->logger->debug((array)$response);

  $searchCriteria = $this->searchCriteriaBuilder
    ->addFilter('increment_id', (int)$response->order_id, 'eq')->create();

  $order = $this->orderRepository->getList($searchCriteria)->getFirstItem();

    switch( $response->status ) {
      case 'ACCEPTED':
        $order->setState(Order::STATE_PROCESSING);
        $order->setStatus('payl8r_accepted');
        $order->addStatusToHistory($order->getStatus(), 'Payment Successful', true);
        $order->save();
        try {
          $this->orderSender->send($order);
        } catch (Exception $e) {}
        break;
      case 'DECLINED':
        $order->setState(Order::STATE_CANCELED);
        $order->setStatus('payl8r_declined');
        $order->addStatusToHistory($order->getStatus(), $response->reason, false);
        $order->save();
        break;
      case 'MANUAL_UNDERWRITING':
          /**
           * Do Nothing i.e. Keeps the order status in payl8r_pending
           *
           * Our platform doesn't usually send these notifications unless the retailers account
           * has been configured to do so.
           */
        break;
      case 'ABANDONED':
      default:
        $order->setState(Order::STATE_CANCELED);
        $order->setStatus('payl8r_abandoned');
        $order->addStatusToHistory($order->getStatus(), $response->reason, false);
        $order->save();
        break;
    }
  }
}
