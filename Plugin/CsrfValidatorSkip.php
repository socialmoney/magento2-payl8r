<?php

namespace Magento\Payl8rPaymentGateway\Plugin;

use Magento\Framework\App\ObjectManager;
use Psr\Log\LoggerInterface;

class CsrfValidatorSkip
{
    /**
     * @param \Magento\Framework\App\Request\CsrfValidator $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\App\ActionInterface $action
     */
    public function aroundValidate(
        $subject,
        \Closure $proceed,
        $request,
        $action
    ) {
        $logger = ObjectManager::getInstance()->get(LoggerInterface::class);
        if ($request->getModuleName() == 'payl8rpaymentgateway') {
            return; // Skip CSRF check
        }
        $proceed($request, $action); // Proceed with Magento 2 CSRF Standard
    }
}