
define(['ko'], function (ko) {
    'use strict';

    var isInAction = ko.observable(false);

    return {
        isInAction: isInAction,

        /**
         * @param {jQuery.Event} event
         */
        stopEventPropagation: function (event) {
            console.log(event)
            if (event.target.id === 'pl-cancel') {

            } else {
                event.stopImmediatePropagation();
                event.preventDefault();
            }
        }
    };
});
